DROP FUNCTION IF EXISTS init_word_sequence();
DROP FUNCTION IF EXISTS destroy_word_sequence();
DROP FUNCTION IF EXISTS add_word_sequence(pos INT, entity BIGINT);
DROP FUNCTION IF EXISTS add_word_sequence_uri(pos INT, entity TEXT);
DROP FUNCTION IF EXISTS init_dependency_graph(maxdist INT, threshold DOUBLE PRECISION, depth INT);
DROP FUNCTION IF EXISTS destroy_dependency_graph();
DROP FUNCTION IF EXISTS compute_centrality();
DROP FUNCTION IF EXISTS word_sense_disambiguation(maxdist INT, threshold DOUBLE PRECISION, depth INT);