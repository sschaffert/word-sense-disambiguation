package io.redlink.disambiguation.algorithm;

import org.apache.commons.io.IOUtils;
import org.openrdf.query.*;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFParseException;
import org.openrdf.sail.memory.MemoryStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class StanbolResultsUtil {

    private static Logger log = LoggerFactory.getLogger(StanbolResultsUtil.class);

    public static List<Collection<String>> getWordSequence(InputStream stanbolResult, RDFFormat format) throws RepositoryException, IOException, RDFParseException {

        String query = IOUtils.toString(StanbolResultsUtil.class.getResourceAsStream("/word_sequence.sparql"));

        Repository repository = new SailRepository(new MemoryStore());
        repository.initialize();

        ArrayList<Collection<String>> result = new ArrayList<>();

        RepositoryConnection con = repository.getConnection();
        try {
            con.begin();

            con.add(stanbolResult, "http://localhost/", format);

            try {
                TupleQuery tq = con.prepareTupleQuery(QueryLanguage.SPARQL, query);

                TupleQueryResult r = tq.evaluate();
                int lastPos = -1;
                while(r.hasNext()) {
                    BindingSet row = r.next();

                    int newPos = Integer.parseInt(row.getValue("start").stringValue());

                    Collection<String> entities;
                    if(newPos != lastPos) {
                        entities = new HashSet<>();
                        result.add(entities);
                    } else {
                        entities = result.get(result.size()-1);
                    }
                    entities.add(row.getValue("entity").stringValue());

                    lastPos = newPos;
                }

            } catch (MalformedQueryException e) {
                log.error("could not parse SPARQL query",e);
            } catch (QueryEvaluationException e) {
                log.error("could not evaluate SPARQL query", e);
            }


            con.commit();
        } finally {
            con.close();
        }

        return result;

    }

    public static void main(String[] args) throws RepositoryException, RDFParseException, IOException {
        System.out.println(getWordSequence(StanbolResultsUtil.class.getResourceAsStream("/disambiguation1.turtle"), RDFFormat.TURTLE));
    }


}
