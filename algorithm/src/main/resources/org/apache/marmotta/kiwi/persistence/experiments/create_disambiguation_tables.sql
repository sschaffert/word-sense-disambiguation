-- Licensed to the Apache Software Foundation (ASF) under one or more
-- contributor license agreements.  See the NOTICE file distributed with
-- this work for additional information regarding copyright ownership.
-- The ASF licenses this file to You under the Apache License, Version 2.0
-- (the "License"); you may not use this file except in compliance with
-- the License.  You may obtain a copy of the License at
--
--      http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
CREATE TABLE weights (
  property   bigint            REFERENCES nodes(id),
  weight     double precision  NOT NULL DEFAULT 0.0,
  PRIMARY KEY(property)
);

CREATE INDEX idx_triples_o ON TRIPLES(object);

CREATE TABLE shortest_paths (
  fromId BIGINT REFERENCES nodes(id),
  toId   BIGINT REFERENCES nodes(id),
  weight DOUBLE PRECISION
);

CREATE INDEX idx_shortest_path ON shortest_paths(fromId,toId);


