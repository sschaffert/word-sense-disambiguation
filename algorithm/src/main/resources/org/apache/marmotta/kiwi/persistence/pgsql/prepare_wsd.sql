
-- ------------------------------------------------------------------------------------------------------------------
-- Word Sense Disambiguation (http://www.cse.unt.edu/~rada/papers/sinha.ieee07.pdf)
-- ------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION init_word_sequence() RETURNS VOID AS $$
BEGIN
  CREATE TEMPORARY TABLE word_sequence (
    position INT,
    entity   BIGINT,
    score    DOUBLE PRECISION
  );
  CREATE INDEX idx_word_sequence_position ON word_sequence(position,entity);
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION destroy_word_sequence() RETURNS VOID AS $$
BEGIN
  DROP TABLE word_sequence;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION add_word_sequence(pos INT, entity BIGINT) RETURNS VOID AS $$
BEGIN
  INSERT INTO word_sequence(position,entity) VALUES (pos, entity);
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION add_word_sequence_uri(pos INT, entity TEXT) RETURNS VOID AS $$
BEGIN
  INSERT INTO word_sequence(position,entity) VALUES (pos, (SELECT id FROM nodes WHERE ntype = 'uri' AND svalue = entity));
END;
$$ LANGUAGE plpgsql;

-- initialise label dependency graph according to algorithm in http://www.cse.unt.edu/~rada/papers/sinha.ieee07.pdf
CREATE OR REPLACE FUNCTION init_dependency_graph(maxdist INT, threshold DOUBLE PRECISION, depth INT) RETURNS VOID AS $$
DECLARE
  s BIGINT;
  t BIGINT;
  w DOUBLE PRECISION;

  minpos INT;
  maxpos INT;
BEGIN
  CREATE TEMPORARY TABLE dependency_graph (
    fromNode BIGINT,
    toNode   BIGINT,
    weight   DOUBLE PRECISION,
    PRIMARY KEY (fromNode,toNode)
  );

  SELECT min(position) INTO minpos FROM word_sequence;
  SELECT max(position) INTO maxpos FROM word_sequence;

  FOR i IN minpos..maxpos LOOP
    FOR j IN i+1..maxpos LOOP
      IF j - i > maxdist THEN
        EXIT;
      END IF;

      INSERT INTO dependency_graph (
        SELECT T.entity, S.entity, shortest_path_ids(S.entity,T.entity,threshold,depth)
        FROM word_sequence S, word_sequence T WHERE S.position = i AND T.position = j
      );

     END LOOP;
  END LOOP;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION destroy_dependency_graph() RETURNS VOID AS $$
BEGIN
  DROP TABLE dependency_graph;
END;
$$ LANGUAGE plpgsql;


-- compute centrality
CREATE OR REPLACE FUNCTION compute_centrality() RETURNS VOID AS $$
BEGIN
  CREATE TEMPORARY TABLE node_centrality (
    nodeId BIGINT,
    weight DOUBLE PRECISION
  );

  INSERT INTO node_centrality (SELECT fromNode AS nodeId, weight FROM dependency_graph);
  INSERT INTO node_centrality (SELECT toNode AS nodeId, weight FROM dependency_graph);

  UPDATE word_sequence
  SET score = S.score
  FROM (SELECT nodeId, sum(weight) AS score FROM node_centrality GROUP BY nodeId) AS S
  WHERE entity = S.nodeId;

  DROP TABLE node_centrality;

END;
$$ LANGUAGE plpgsql;


-- main method
CREATE OR REPLACE FUNCTION word_sense_disambiguation(maxdist INT, threshold DOUBLE PRECISION, depth INT) RETURNS VOID AS $$
BEGIN
  PERFORM init_dependency_graph(maxdist, threshold, depth);
  PERFORM compute_centrality();
  PERFORM destroy_dependency_graph();
END;
$$ LANGUAGE plpgsql;


-- sample:
-- SELECT init_word_sequence();
-- SELECT add_word_sequence_uri(1,'http://dbpedia.org/resource/Paris');
-- SELECT add_word_sequence_uri(1,'http://dbpedia.org/resource/Paris,_Texas');
-- SELECT add_word_sequence_uri(1,'http://dbpedia.org/resource/Paris_Hilton');
-- SELECT add_word_sequence_uri(2,'http://dbpedia.org/resource/France');
-- SELECT add_word_sequence_uri(2,'http://dbpedia.org/resource/France,_Southern');
-- SELECT add_word_sequence_uri(3,'http://dbpedia.org/resource/Bordeaux');
-- SELECT add_word_sequence_uri(3,'http://dbpedia.org/resource/Bordeaux,_United_States_Virgin_Islands');
--
-- SELECT word_sense_disambiguation(3,0.01,2);
--
-- SELECT W.position, W.entity, N.svalue AS uri, B.best
-- FROM (SELECT position, max(score) AS best FROM word_sequence GROUP BY position) B, word_sequence W, nodes N
-- WHERE B.position = W.position AND B.best = W.score AND N.id = W.entity;


-- query for best matches:


-- SELECT W.position, W.entity, N.svalue AS uri, B.best
-- FROM (SELECT position, max(score) AS best FROM word_sequence GROUP BY position) B, word_sequence W, nodes N
-- WHERE B.position = W.position AND B.best = W.score AND N.id = W.entity;

-- more efficient

-- SELECT DISTINCT ON(position) position, entity, last_value(score) OVER wnd AS score
-- FROM word_sequence
-- WINDOW wnd AS (
--   PARTITION BY position ORDER BY score ASC
--   ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
-- )


-- or without URI:

-- SELECT W.position, W.entity B.best
-- FROM (SELECT position, max(score) AS best FROM word_sequence GROUP BY position) B, word_sequence W
-- WHERE B.position = W.position AND B.best = W.score;

-- or for a single position:
-- SELECT entity, score FROM word_sequence WHERE position = ? ORDER BY score DESC