package io.redlink.disambiguation.algorithm;

import io.redlink.disambiguation.model.EntityScore;
import org.apache.marmotta.kiwi.caching.KiWiCacheManager;
import org.apache.marmotta.kiwi.persistence.KiWiConnection;
import org.apache.marmotta.kiwi.persistence.KiWiDialect;
import org.apache.marmotta.kiwi.persistence.KiWiPersistence;
import org.apache.marmotta.kiwi.persistence.util.ScriptRunner;
import org.infinispan.Cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Wrap a KiWiConnection and carry out a shortest path computation based on a
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class ShortestPathConnection {

    private static Logger log = LoggerFactory.getLogger(ShortestPathConnection.class);

    private KiWiConnection conn;

    public ShortestPathConnection(KiWiConnection conn) throws SQLException {
        this.conn = conn;
    }


    /**
     * Prepare the database for word sense disambiguation. Only call this method if the database has not yet been
     * prepared or the disambiguation data structures need to be re-initialised.
     *
     * @throws SQLException
     */
    public void prepareDatabase() throws SQLException {
        log.info("preparing database for word sense disambiguation ...");
        initWeights();
        initSelectivities();
        initShortestPath();
        initWSD();
    }


    /**
     * Initialise the weights table by listing all properties currently existing in the database and assigning the
     * default value (0.5) as weight.
     *
     * @throws SQLException
     */
    public void initWeights() throws SQLException {
        log.info("- initialising property weights ...");
        runScript("prepare_weights.sql");
    }

    /**
     * Set or update the weight of the property with the given URI to the given double value (should be between 0 and 1).
     * Changing the weight to 0.0 effectively eliminates a predicate from being considered in shortest path computations
     * while changing the weight to 1.0 represents equality of two concepts.
     *
     * @param propertyUri
     * @param weight
     * @throws SQLException
     */
    public void setWeight(String propertyUri, double weight) throws SQLException {
        PreparedStatement get = conn.getPreparedStatement("weights.get");
        get.setString(1, propertyUri);

        try(ResultSet getResult = get.executeQuery()) {
            if(getResult.next()) {
                PreparedStatement upd = conn.getPreparedStatement("weights.update");
                upd.setDouble(1, weight);
                upd.setString(2, propertyUri);
                upd.executeUpdate();
            } else {
                PreparedStatement set = conn.getPreparedStatement("weights.store");
                set.setString(1, propertyUri);
                set.setDouble(2, weight);
                set.executeUpdate();
            }
        }
    }

    /**
     * Return the weight of the property with the given URI (or 0.0 in case the property has no weight associated).
     *
     * @param propertyUri
     * @return
     * @throws SQLException
     */
    public double getWeight(String propertyUri) throws SQLException {
        PreparedStatement get = conn.getPreparedStatement("weights.get");
        get.setString(1, propertyUri);

        try(ResultSet getResult = get.executeQuery()) {
            if(getResult.next()) {
                return getResult.getDouble(1);
            } else {
                return 0.0;
            }
        }
    }

    /**
     * Drop the current weights table, removing the weights for all properties.
     *
     * @throws SQLException
     */
    public void dropWeights() throws SQLException {
        log.info("- removing property weights ...");
        runScript("drop_weights.sql");
    }


    /**
     * (Re-)initialise the table storing statistical information about how selective a parent node is when disambiguating
     * two concepts. The reason for this table is to "punish" nodes with many incoming edges, because they contribute
     * little to determine how related two concepts are.
     * <p/>
     * When the triple table is updated frequently, the selectivities need to be re-initialised from time to time to
     * reflect changed node statistics.
     *
     * @throws SQLException
     */
    public void initSelectivities() throws SQLException {
        log.info("- initialising node statistics ...");
        runScript("prepare_selectivities.sql");
    }

    /**
     * Drop the selectivity information currently stored in the database. Only really necessary for cleanup in case
     * the database will no more be used for disambiguation.
     *
     * @throws SQLException
     */
    public void dropSelectivities() throws SQLException {
        log.info("- removing node statistics ...");
        runScript("drop_selectivities.sql");
    }


    /**
     * Initialise the stored procedures for computing shortest paths in the database.
     *
     * @throws SQLException
     */
    public void initShortestPath() throws SQLException {
        log.info("- initialising shortest path functions ...");
        runScript("prepare_shortest_path.sql");
    }

    /**
     * Drop the stored procedures and cache table used for computing shortest paths in the database. Only really
     * necessary for cleanup in case the database will no more be used for disambiguation.
     * @throws SQLException
     */
    public void dropShortestPath() throws SQLException {
        log.info("- removing shortest path functions ...");
        runScript("drop_shortest_path.sql");
    }


    /**
     * Remove all cache entries from the shortest path cache. Should be called when the triplestore was updated
     * to reflect better alternatives.
     *
     * @throws SQLException
     */
    public void clearShortestPathCache() throws SQLException {
        log.info("- clearing shortest path cache ...");
        PreparedStatement upd = conn.getPreparedStatement("update.weights");
        upd.executeUpdate();
    }


    /**
     * Initialise the stored procedures used for computing word sense disambiguation in the database.
     *
     * @throws SQLException
     */
    public void initWSD() throws SQLException {
        log.info("- initialising disambiguation functions ...");
        runScript("prepare_wsd.sql");
    }


    /**
     * Drop the stored procedures used for computing word sense disambiguation in the database.  Only really
     * necessary for cleanup in case the database will no more be used for disambiguation.
     *
     * @throws SQLException
     */
    public void dropWSD() throws SQLException {
        log.info("- removing disambiguation functions ...");
        runScript("drop_wsd.sql");
    }


    /**
     * Compute the disambiguation for a sequence of words, each with a collection of entity URIs and return the best
     * candidates for each word in the sequence. Computation follows the word sense disambiguation algorithm described
     * in <a href="http://www.cse.unt.edu/~rada/papers/sinha.ieee07.pdf">this paper</a>.
     * <p/>
     * The wordSequence is an ordered list where each element represents a word occurring in a text at the same position.
     * Each element is the list of URIs of candidate entities that match the word.
     * <p/>
     * The windowSize limits the size of the context to consider for disambiguation. Weights in the dependency graph
     * below the threshold are considered 0.0. The maxDistance limits the distance (2*maxDistance) to take into account
     * for shortest path computations over the underlying triple store.
     * <p/>
     *     Reasonable values seem to be:
     * <ul>
     *     <li>windowSize: 3</li>
     *     <li>threshold:  0.01</li>
     *     <li>maxDistance: 2</li>
     * </ul>
     * The computation will return a list of optimal entity scores as determined by this algorithm, in the same order
     * as the original word sequence.
     *
     *
     * @param wordSequence  ordered list where each element is the collection of URIs of candidate entities for the
     *                      word at this position
     * @param windowSize    size of the context to consider for disambiguation
     * @param threshold     minimum weight for a relatedness value to take into account
     * @param maxDistance   maximum path distance for shortest paths / 2
     * @param context
     * @return  list of optimal entity scores as determined by this algorithm, in the same order as the original word sequence.
     */
    public List<EntityScore> computeDisambiguationBest(List<Collection<String>> wordSequence, int windowSize, double threshold, int maxDistance, String context) throws SQLException {
        // prepare temporary table
        PreparedStatement init = conn.getPreparedStatement("wsd.init");
        init.execute();

        // load all entity candidates into the temporary table
        PreparedStatement load = conn.getPreparedStatement("wsd.add");
        load.clearBatch();
        for(int i=0; i<wordSequence.size(); i++) {
            for(String uri : wordSequence.get(i)) {
                load.clearParameters();
                load.setInt(1,i);
                load.setString(2, uri);
                load.addBatch();
            }
        }
        load.executeBatch();

        // compute word sense disambiguation
        PreparedStatement compute = conn.getPreparedStatement("wsd.compute");
        compute.setInt(1,windowSize);
        compute.setDouble(2, threshold);
        compute.setInt(3,maxDistance);
        compute.execute();

        ArrayList<EntityScore> result = new ArrayList<>(wordSequence.size());

        // retrieve results
        PreparedStatement retrieve = conn.getPreparedStatement("wsd.retrieve");
        try(ResultSet r = retrieve.executeQuery()) {
            while(r.next()) {
                result.add(new EntityScore(r.getString("uri"), r.getDouble("best")));
            }
        }

        PreparedStatement destroy = conn.getPreparedStatement("wsd.destroy");
        destroy.execute();

        return result;
    }



    /**
     * Compute the disambiguation for a sequence of words, each with a collection of entity URIs and assign scores to
     * all candidates for further processing. Computation follows the word sense disambiguation algorithm described
     * in <a href="http://www.cse.unt.edu/~rada/papers/sinha.ieee07.pdf">this paper</a>.
     * <p/>
     * The wordSequence is an ordered list where each element represents a word occurring in a text at the same position.
     * Each element is the list of URIs of candidate entities that match the word.
     * <p/>
     * The windowSize limits the size of the context to consider for disambiguation. Weights in the dependency graph
     * below the threshold are considered 0.0. The maxDistance limits the distance (2*maxDistance) to take into account
     * for shortest path computations over the underlying triple store.
     * <p/>
     *     Reasonable values seem to be:
     * <ul>
     *     <li>windowSize: 3</li>
     *     <li>threshold:  0.01</li>
     *     <li>maxDistance: 2</li>
     * </ul>
     * The computation will return a list of lists of entity scores as determined by this algorithm, in the same order
     * as the original word sequence.
     *
     *
     * @param wordSequence  ordered list where each element is the collection of URIs of candidate entities for the
     *                      word at this position
     * @param windowSize    size of the context to consider for disambiguation
     * @param threshold     minimum weight for a relatedness value to take into account
     * @param maxDistance   maximum path distance for shortest paths / 2
     * @param context
     * @return  list where each element is the collection of URIs with their assigned scores as determined by this
     *          algorithm, in the same order as the original word sequence.
     */
    public List<List<EntityScore>> computeDisambiguationScore(List<Collection<String>> wordSequence, int windowSize, double threshold, int maxDistance, String context) throws SQLException {
        // prepare temporary table
        PreparedStatement init = conn.getPreparedStatement("wsd.init");
        init.execute();

        // load all entity candidates into the temporary table
        PreparedStatement load = conn.getPreparedStatement("wsd.add");
        load.clearBatch();
        for(int i=0; i<wordSequence.size(); i++) {
            for(String uri : wordSequence.get(i)) {
                load.clearParameters();
                load.setInt(1,i);
                load.setString(2, uri);
                load.addBatch();
            }
        }
        load.executeBatch();

        // compute word sense disambiguation
        PreparedStatement compute = conn.getPreparedStatement("wsd.compute");
        compute.setInt(1,windowSize);
        compute.setDouble(2, threshold);
        compute.setInt(3,maxDistance);
        compute.execute();

        ArrayList<List<EntityScore>> result = new ArrayList<>(wordSequence.size());

        // retrieve results
        PreparedStatement retrieve = conn.getPreparedStatement("wsd.score");
        try(ResultSet r = retrieve.executeQuery()) {
            int lastPos = -1;
            while(r.next()) {
                int newPos = r.getInt("position");

                List<EntityScore> scores;
                if(newPos != lastPos) {
                    scores = new ArrayList<>();
                    result.add(scores);
                } else {
                    scores = result.get(result.size() - 1);
                }

                scores.add(new EntityScore(r.getString("uri"), r.getDouble("score")));
            }
        }

        PreparedStatement destroy = conn.getPreparedStatement("wsd.destroy");
        destroy.execute();

        return result;
    }




    private void runScript(String scriptName) throws SQLException {
        Connection c = conn.getJDBCConnection();

        ScriptRunner runner = new ScriptRunner(c, false, true);

        Reader in = new InputStreamReader(this.getClass().getResourceAsStream("/org/apache/marmotta/kiwi/persistence/pgsql/"+scriptName));
        try {
            runner.runScript(in);
        } catch (IOException e) {
            throw new SQLException("could not load script "+scriptName, e);
        }

    }
}
