DROP FUNCTION IF EXISTS shortest_path(fromUri TEXT, toUri TEXT, threshold DOUBLE PRECISION, maxdepth INT);
DROP FUNCTION IF EXISTS shortest_path_ids(fromId BIGINT, toId BIGINT, threshold DOUBLE PRECISION, maxdepth INT);
DROP TABLE IF EXISTS shortest_path_cache;