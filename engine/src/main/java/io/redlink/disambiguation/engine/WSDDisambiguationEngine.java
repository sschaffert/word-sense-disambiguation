package io.redlink.disambiguation.engine;

import io.redlink.disambiguation.algorithm.ShortestPathConnection;
import io.redlink.disambiguation.model.DisambiguationData;
import io.redlink.disambiguation.model.EntityScore;
import io.redlink.disambiguation.model.ExtractedEntity;
import io.redlink.disambiguation.model.Suggestion;
import io.redlink.disambiguation.util.RepositoryTracker;
import io.redlink.disambiguation.util.RepositoryUtil;
import org.apache.clerezza.rdf.core.LiteralFactory;
import org.apache.clerezza.rdf.core.MGraph;
import org.apache.clerezza.rdf.core.Triple;
import org.apache.clerezza.rdf.core.UriRef;
import org.apache.clerezza.rdf.core.impl.TripleImpl;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.*;
import org.apache.marmotta.kiwi.persistence.KiWiConnection;
import org.apache.marmotta.kiwi.sail.KiWiStore;
import org.apache.stanbol.enhancer.servicesapi.*;
import org.apache.stanbol.enhancer.servicesapi.helper.ContentItemHelper;
import org.apache.stanbol.enhancer.servicesapi.helper.EnhancementEngineHelper;
import org.apache.stanbol.enhancer.servicesapi.impl.AbstractEnhancementEngine;
import org.openrdf.repository.Repository;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

import static org.apache.stanbol.enhancer.servicesapi.rdf.Properties.DC_RELATION;

/**
 * Implements a disambiguation using the indegree centrality as described in a paper by Sinha and Mihalcea (2006).
 * The centrality algorithm used is a simple indegree algorithm that computes for each vertice the sum of the weight
 * of all adjacent edges. The weights are determined by a relatedness service which returns a measure for the
 * relatedness between two concepts.
 *
 * <p/>
 * See also <a href="http://www.cse.unt.edu/~rada/papers/sinha.ieee07.pdf">http://www.cse.unt.edu/~rada/papers/sinha.ieee07.pdf</a>
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
@Component(
        configurationFactory = true,
        policy = ConfigurationPolicy.OPTIONAL,
        specVersion = "1.1",
        metatype = true,
        immediate = true,
        inherit = true)
@Service
@org.apache.felix.scr.annotations.Properties(value = {@Property(name = EnhancementEngine.PROPERTY_NAME, value = "disambiguation-wsd")})
public class WSDDisambiguationEngine extends AbstractEnhancementEngine<RuntimeException,RuntimeException> implements ServiceProperties {

    /**
     * The ID of the Sesame Repository used for dereferencing
     */
    @Property(label = "Repository ID", description = "Stanbol ID of the repository to access for disambiguation")
    public static final String REPOSITORY_ID = "org.openrdf.repository.Repository.id";


    @Property(label = "Context URI", description = "URI of the context in the triplestore to access for disambiguation (may be null) - NOT YET SUPPORTED")
    public static final String CONTEXT = "org.apache.stanbol.enhancer.engines.wsd_disambiguation.context";


    @Property(intValue = 3, label = "Window Size", description = "Size of the context to consider around a word for disambiguation")
    public static final String WINDOW_SIZE = "org.apache.stanbol.enhancer.engines.wsd_disambiguation.window_size";

    @Property(doubleValue = 0.01, label = "Path Threshold", description = "Minimum value for a path between concepts to be taken into account.")
    public static final String THRESHOLD = "org.apache.stanbol.enhancer.engines.wsd_disambiguation.threshold";

    @Property(intValue = 2, label = "Path Distance", description = "Maximum distance to compute for a path between concepts in the graph. Actual distance is up to twice this value.")
    public static final String MAX_DISTANCE = "org.apache.stanbol.enhancer.engines.wsd_disambiguation.max_distance";


    private static Logger log = LoggerFactory.getLogger(WSDDisambiguationEngine.class);

    private int windowSize, maximumDistance;

    private double threshold;

    private String context;

    /**
     * The {@link org.apache.clerezza.rdf.core.LiteralFactory} used to create typed RDF literals
     */
    private final LiteralFactory literalFactory = LiteralFactory.getInstance();

    private RepositoryTracker repositoryTracker;


    @SuppressWarnings("unchecked")
    @Activate
    protected void activate(ComponentContext context) throws ConfigurationException {
        super.activate(context);
        Dictionary<String,Object> config = context.getProperties();

        if(config.get(WINDOW_SIZE) != null) {
            windowSize = Integer.parseInt(config.get(WINDOW_SIZE).toString());
        } else {
            windowSize = 3;
        }

        if(config.get(MAX_DISTANCE) != null) {
            maximumDistance = Integer.parseInt(config.get(MAX_DISTANCE).toString());
        } else {
            maximumDistance = 2;
        }

        if(config.get(THRESHOLD) != null) {
            threshold = Double.parseDouble(config.get(MAX_DISTANCE).toString());
        } else {
            threshold = 0.01;
        }


        if(config.get(CONTEXT) != null) {
            this.context = config.get(CONTEXT).toString();
        }

        final Map<String,String> filterProperties;
        Object value = config.get(REPOSITORY_ID);
        if (value == null || StringUtils.isBlank(value.toString())) {
            filterProperties = Collections.emptyMap();
        } else {
            filterProperties = Collections.singletonMap(REPOSITORY_ID, value.toString());
        }

        repositoryTracker = new RepositoryTracker(context.getBundleContext(), filterProperties, null);
    }

    @Deactivate
    protected void deactivate(ComponentContext context) {
        repositoryTracker.close();
    }

    /**
     * Getter for the properties defined by this service.
     *
     * @return An unmodifiable map of properties defined by this service
     */
    @Override
    public Map<String, Object> getServiceProperties() {
        return Collections.<String,Object>singletonMap(ServiceProperties.ENHANCEMENT_ENGINE_ORDERING, ServiceProperties.ORDERING_POST_PROCESSING);
    }

    /**
     * Indicate if this engine can enhance supplied ContentItem, and if it
     * suggests enhancing it synchronously or asynchronously. The
     * {@link org.apache.stanbol.enhancer.servicesapi.EnhancementJobManager} can force sync/async mode if desired, it is
     * just a suggestion from the engine.
     *
     * @throws org.apache.stanbol.enhancer.servicesapi.EngineException
     *          if the introspecting process of the content item
     *          fails
     */
    @Override
    public int canEnhance(ContentItem ci) throws EngineException {
        // check if relatedness service is present
        if(repositoryTracker.getRepository() == null) {
            log.error("relatedness service is not available; cannot enhance content");
            return CANNOT_ENHANCE;
        }


        // check if content is present
        try {
            if ((ContentItemHelper.getText(ci.getBlob()) == null)
                    || (ContentItemHelper.getText(ci.getBlob()).trim().isEmpty())) {
                return CANNOT_ENHANCE;
            }
        } catch (IOException e) {
            log.error("Failed to get the text for " + "enhancement of content: " + ci.getUri(), e);
            throw new InvalidContentException(this, ci, e);
        }
        // default enhancement is synchronous enhancement
        return ENHANCE_SYNCHRONOUS;
    }

    /**
     * Implements a disambiguation using the indegree centrality as described in a paper by Sinha and Mihalcea (2006).
     * The centrality algorithm used is a simple indegree algorithm that computes for each vertice the sum of the weight
     * of all adjacent edges. The weights are determined by a relatedness service which returns a measure for the
     * relatedness between two concepts.
     *
     * <p/>
     * See also <a href="http://www.cse.unt.edu/~rada/papers/sinha.ieee07.pdf">http://www.cse.unt.edu/~rada/papers/sinha.ieee07.pdf</a>
     */
    @Override
    public void computeEnhancements(ContentItem ci) throws EngineException {
        // get disambiguation data from content item as starting point for computation
        DisambiguationData data;

        ci.getLock().readLock().lock();
        try {
            data = DisambiguationData.createFromContentItem(ci);
        } finally {
            ci.getLock().readLock().unlock();
        }

        Repository repository = repositoryTracker.getRepository();
        KiWiStore  store = RepositoryUtil.getKiWiStore(repository);
        try {
            KiWiConnection con = store.getPersistence().getConnection();
            try {
                ShortestPathConnection sp = new ShortestPathConnection(con);
                data.applyEntityScores(sp.computeDisambiguationScore(data.asUriList(), windowSize, threshold, maximumDistance, context));

                con.commit();

                ci.getLock().writeLock().lock();
                try {
                    applyDisambiguationResults(ci.getMetadata(), data);
                } finally {
                    ci.getLock().writeLock().unlock();
                }

            } finally {
                con.close();
            }
        } catch (SQLException e) {
            throw new EngineException("error connecting to triplestore");
        }


    }

    /**
     * Adds the disambiguation results to the enhancement structure
     *
     * @param graph
     *            the metadata of the {@link ContentItem}
     * @param disData
     *            the disambiguation data
     */
    protected void applyDisambiguationResults(MGraph graph, DisambiguationData disData) {
        // TODO: we should use the normal confidence property in the future!
        UriRef ENHANCER_CONFIDENCE = new UriRef("http://dev.redlink.io/disambiguation/confidence");

        for (ExtractedEntity savedEntity : disData.textAnnotations.values()) {
            for (Suggestion s : savedEntity.getSuggestions()) {
                if (s.getDisambiguatedConfidence() != null) {
                    if (disData.suggestionMap.get(s.getEntityAnnotation()).size() > 1) {
                        // already encountered AND disambiguated -> we need to clone!!
                        log.info("clone {} suggesting {} for {}[{},{}]({})",
                                new Object[] {s.getEntityAnnotation(), s.getEntityUri(), savedEntity.getName(),
                                        savedEntity.getStart(), savedEntity.getEnd(), savedEntity.getUri()});
                        s.setEntityAnnotation(cloneTextAnnotation(graph, s.getEntityAnnotation(),
                                savedEntity.getUri()));
                        log.info("  - cloned {}", s.getEntityAnnotation());
                    }
                    // change the confidence
                    EnhancementEngineHelper.set(graph, s.getEntityAnnotation(), ENHANCER_CONFIDENCE, s.getDisambiguatedConfidence(), literalFactory);
                    EnhancementEngineHelper.addContributingEngine(graph, s.getEntityAnnotation(), this);
                }
            }
        }
    }

    /**
     * This creates a 'clone' of the fise:EntityAnnotation where the original does no longer have a
     * dc:relation to the parsed fise:TextAnnotation and the created clone does only have a dc:relation to the
     * parsed fise:TextAnnotation.
     * <p>
     * This is required by disambiguation because other engines typically only create a single
     * fise:EntityAnnotation instance if several fise:TextAnnotation do have the same fise:selected-text
     * values. So for a text that multiple times mentions the same Entity (e.g. "Paris") there will be
     * multiple fise:TextAnnotations selecting the different mentions of that Entity, but there will be only a
     * single set of suggestions - fise:EntityAnnotations (e.g. "Paris, France" and "Paris, Texas"). Now lets
     * assume a text like
     *
     * <pre>
     *     Paris is the capital of France and it is worth a visit for sure. But
     *     one can also visit Paris without leaving the United States as there
     *     is also a city with the same name in Texas.
     * </pre>
     *
     * Entity Disambiguation need to be able to have different fise:confidence values for the first and second
     * mention of Paris and this is only possible of the fise:TextAnnotations of those mentions do NOT refer
     * to the same set of fise:EntityAnnotations.
     * <p>
     * This methods accomplished exactly that as it
     * <ul>
     * <li>creates a clone of a fise:EntityAnnotation
     * <li>removes the dc:relation link to the 2nd mention of Paris from the original
     * <li>only adds the dc:relation of the end mention to the clone
     * </ul>
     * So in the end you will have two fise:EntityAnnotation
     * <ul>
     * <li>the original fise:EntityAnnotation with dc:relation to all fise:TextAnnotations other than the 2nd
     * mention (the one this method was called for)
     * <li>the cloned fise:EntityAnnnotation with a dc:relation to the 2nd mention.
     * </ul>
     *
     * @param graph
     * @param entityAnnotation
     * @param textAnnotation
     * @return
     */
    public static UriRef cloneTextAnnotation(MGraph graph, UriRef entityAnnotation, UriRef textAnnotation) {
        UriRef copy = new UriRef("urn:enhancement-" + EnhancementEngineHelper.randomUUID());
        Iterator<Triple> it = graph.filter(entityAnnotation, null, null);
        // we can not add triples to the graph while iterating. So store them
        // in a list and add later
        List<Triple> added = new ArrayList<Triple>(32);
        while (it.hasNext()) {
            Triple triple = it.next();
            if (DC_RELATION.equals(triple.getPredicate())) {
                if (triple.getObject().equals(textAnnotation)) {
                    // remove the dc relation to the currently processed
                    // textAnnotation from the original
                    it.remove();
                    // and add it to the copy
                    added.add(new TripleImpl(copy, // use the copy as subject!
                            triple.getPredicate(), triple.getObject()));
                } // else it is not the currently processed TextAnnotation
                // so we need to keep in in the original and NOT add
                // it to the copy
            } else { // we can copy all other information 1:1
                added.add(new TripleImpl(copy, // use the copy as subject!
                        triple.getPredicate(), triple.getObject()));
            }
        }
        graph.addAll(added);
        return copy;
    }

}
