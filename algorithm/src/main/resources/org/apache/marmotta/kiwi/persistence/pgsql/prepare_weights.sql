-- ------------------------------------------------------------------------------------------------------------------
-- the weights table contains the node ids of properties together with the relative weight with which they contribute
-- to a "best path" (between 0 and 1).
-- ------------------------------------------------------------------------------------------------------------------
DROP TABLE IF EXISTS weights;

CREATE TABLE weights (
  property   bigint            REFERENCES nodes(id),
  weight     double precision  NOT NULL DEFAULT 0.0,
  PRIMARY KEY(property)
);

-- initialise weights table; afterwards weights should be adjusted according to how strong a relation should be
INSERT INTO weights (SELECT DISTINCT T.predicate, 0.5 FROM triples T, nodes N WHERE N.id = T.object AND N.ntype = 'uri');