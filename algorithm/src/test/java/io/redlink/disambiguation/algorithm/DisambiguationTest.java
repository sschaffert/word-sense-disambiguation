package io.redlink.disambiguation.algorithm;

import io.redlink.disambiguation.model.EntityScore;
import org.apache.marmotta.kiwi.config.KiWiConfiguration;
import org.apache.marmotta.kiwi.persistence.KiWiConnection;
import org.apache.marmotta.kiwi.persistence.pgsql.PostgreSQLDialect;
import org.apache.marmotta.kiwi.sail.KiWiStore;
import org.junit.*;
import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFParseException;
import org.openrdf.sail.SailException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class DisambiguationTest {

    private static Logger log = LoggerFactory.getLogger(DisambiguationTest.class);

    private static KiWiStore store;

    private KiWiConnection conn;

    @BeforeClass
    public static void setup() throws SailException {

        KiWiConfiguration cfg = new KiWiConfiguration("dbpedia","jdbc:postgresql://accounting.bb.redlink.io:5432/dbpedia","marmotta","JoaoGomes", new PostgreSQLDialect());

        store = new KiWiStore(cfg);
        store.initialize();

    }


    @AfterClass
    public static void shutdown() throws SailException {
        store.shutDown();
    }


    @Before
    public void initConnection() throws SQLException {
        conn = store.getPersistence().getConnection();
    }

    @After
    public void closeConnection() throws SQLException {
        conn.close();
    }


    @Test
    public void testDisambiguation1() throws RepositoryException, SQLException, RDFParseException, IOException {
        testDisambiguation("/disambiguation1.turtle", new String[] {"http://dbpedia.org/resource/Seattle", "http://dbpedia.org/resource/Washington_(state)", "http://dbpedia.org/resource/Portland,_Oregon"} );
    }

    @Test
    public void testDisambiguation2() throws RepositoryException, SQLException, RDFParseException, IOException {
        testDisambiguation("/disambiguation2.turtle", new String[] {
                "http://dbpedia.org/resource/Geelong_Football_Club",
                "http://dbpedia.org/resource/Gold_Coast_Football_Club",
                "http://dbpedia.org/resource/Josh_Caddy",
                "http://dbpedia.org/resource/David_Cameron_(footballer)",
                "http://dbpedia.org/resource/Geelong_Football_Club",
                "http://dbpedia.org/resource/Melbourne_Football_Club",
                "http://dbpedia.org/resource/Carlton_Football_Club"
        } );
    }


    private void testDisambiguation(String file, String[] expected) throws SQLException, RepositoryException, RDFParseException, IOException {
        ShortestPathConnection sp = new ShortestPathConnection(conn);

        List<Collection<String>> wordSequence = StanbolResultsUtil.getWordSequence(this.getClass().getResourceAsStream(file), RDFFormat.TURTLE);

        log.info("word sequence: {}", wordSequence);

        long start = System.currentTimeMillis();
        List<EntityScore> result = sp.computeDisambiguationBest(wordSequence, 3, 0.01, 2, null);

        log.info("result ({} ms): {}", System.currentTimeMillis() - start, result);

        for(int i=0; i<expected.length; i++) {
            Assert.assertEquals("entity at position "+i+" not properly disambiguated", expected[i], result.get(i).getUri());
        }
    }
}
