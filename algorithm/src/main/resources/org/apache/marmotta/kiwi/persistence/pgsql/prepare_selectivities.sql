-- ------------------------------------------------------------------------------------------------------------------
-- create a table with relative selectivity of a node with respect to a property and run a calculation over the triples
-- table to precompute these values
-- NOTE: needs to be recomputed in case the graph is changed
-- ------------------------------------------------------------------------------------------------------------------
DROP TABLE IF EXISTS selectivities;

SELECT N.id, T.predicate AS property, 1/(log(count(T.id) - 1)+1) AS selectivity
INTO UNLOGGED selectivities
FROM nodes N, triples T
WHERE T.object = N.id AND N.ntype = 'uri'  GROUP BY N.id, T.predicate HAVING count(T.id) > 1;

CREATE INDEX idx_selectivities ON selectivities(id,property);