package io.redlink.disambiguation.util;

import org.apache.marmotta.kiwi.sail.KiWiStore;
import org.openrdf.repository.Repository;
import org.openrdf.repository.base.RepositoryWrapper;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.sail.Sail;
import org.openrdf.sail.StackableSail;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class RepositoryUtil {



    public static KiWiStore getKiWiStore(Repository repository) {
        Repository current = repository;
        while(current != null && current instanceof RepositoryWrapper) {
            current = ((RepositoryWrapper) current).getDelegate();
        }
        if(current instanceof SailRepository) {
            return getKiWiStore(((SailRepository) current).getSail());
        } else {
            throw new IllegalStateException("the base store is not a KiWiStore (type: "+current.getClass().getCanonicalName()+")!");
        }
    }


    public static KiWiStore getKiWiStore(Sail sail) {
        Sail current = sail;
        while(current != null && current instanceof StackableSail) {
            current = ((StackableSail) current).getBaseSail();
        }
        if(current != null && current instanceof KiWiStore) {
            return (KiWiStore) current;
        } else {
            throw new IllegalStateException("the base store is not a KiWiStore (type: "+current.getClass().getCanonicalName()+")!");
        }

    }

}
