CREATE TABLE materialized_paths (
  fromId BIGINT REFERENCES nodes(id),
  toId   BIGINT REFERENCES nodes(id),
  weight DOUBLE PRECISION,
  PRIMARY KEY (fromId,toId)
);

CREATE OR REPLACE FUNCTION floyd_warshall(threshold DOUBLE PRECISION) RETURNS VOID AS $$
DECLARE
    weights      RECORD;
    oldweight    DOUBLE PRECISION := 0.0;
    k            BIGINT;
    i            BIGINT;
    j            BIGINT;
    dist_ij      DOUBLE PRECISION;
    dist_ik      DOUBLE PRECISION;
    dist_kj      DOUBLE PRECISION;

BEGIN
  -- init
  CREATE TEMPORARY TABLE shortest_paths(
    fromId BIGINT,
    toId   BIGINT,
    weight DOUBLE PRECISION,
    PRIMARY KEY (fromId,toId)
  );

  CREATE TEMPORARY TABLE uri_nodes AS (SELECT id FROM nodes WHERE ntype = 'uri');

  FOR weights IN SELECT T.subject, T.object, W.weight
                 FROM triples T, weights W
                 WHERE T.deleted = false AND T.predicate = W.property AND W.weight > threshold
  LOOP
    PERFORM update_shortest_path(weights.subject, weights.object, weights.weight);
    PERFORM update_shortest_path(weights.object, weights.subject, weights.weight);
  END LOOP;

  RAISE NOTICE 'initialisation completed';

  ANALYZE shortest_paths;

  -- loop
  FOR k IN SELECT id FROM uri_nodes
  LOOP
    FOR i IN SELECT id FROM uri_nodes
    LOOP
      dist_ik := get_shortest_path(i,k);

      IF dist_ik > 0 THEN
        FOR j IN SELECT id FROM uri_nodes
        LOOP
          dist_kj := get_shortest_path(k,j);

          IF dist_kj > 0 AND dist_ik * dist_kj > threshold THEN
            dist_ij := get_shortest_path(i,j);

            IF dist_ij < dist_ik * dist_kj THEN
              PERFORM update_shortest_path(i,j,dist_ik * dist_kj);
            END IF;
          END IF ;
        END LOOP;
      END IF;
    END LOOP;
  END LOOP;

  RAISE NOTICE 'computation completed, inserting into materialized table';

  INSERT INTO materialized_paths SELECT * FROM shortest_paths;

  RAISE NOTICE 'finished';

  DROP TABLE shortest_paths;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION update_shortest_path(fId BIGINT, tId BIGINT, wt DOUBLE PRECISION) RETURNS VOID AS $$
DECLARE
    oldweight    DOUBLE PRECISION := 0.0;

BEGIN
    UPDATE shortest_paths SET weight = wt WHERE fromId = fId AND toId = tId;
    IF NOT FOUND THEN
      INSERT INTO shortest_paths(fromId, toId, weight) VALUES (fId, tId, wt);
    END IF;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION get_shortest_path(fId BIGINT, tId BIGINT) RETURNS DOUBLE PRECISION AS $$
DECLARE
  result    DOUBLE PRECISION;

BEGIN
  SELECT weight INTO result FROM shortest_paths WHERE fromId = fId AND toId = tId;
  IF NOT FOUND THEN
    return 0.0;
  ELSE
    return result;
  END IF;
END;
$$ LANGUAGE plpgsql;
