package io.redlink.disambiguation.model;

/**
 * The score of an entity as found by disambiguation.
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class EntityScore {

    private String uri;

    private double score;

    public EntityScore(String uri, double score) {
        this.uri = uri;
        this.score = score;
    }

    public String getUri() {
        return uri;
    }

    public double getScore() {
        return score;
    }

    @Override
    public String toString() {
        return "EntityScore{" +
                "uri='" + uri + '\'' +
                ", score=" + score +
                '}';
    }
}
