-- ------------------------------------------------------------------------------------------------------------------
-- the stored procedures below are capable of caching shortest path computations in this table;
-- NOTE: the cache needs to be invalidated in case the graph is changed
-- ------------------------------------------------------------------------------------------------------------------
DROP TABLE IF EXISTS shortest_path_cache;

CREATE UNLOGGED TABLE shortest_path_cache (
  fromNode BIGINT,
  toNode   BIGINT,
  weight   DOUBLE PRECISION,
  PRIMARY KEY (fromNode,toNode)
);


-- ------------------------------------------------------------------------------------------------------------------
-- Compute shortest path between two URIs.
-- fromUri; URI of the source node to start the computation at
-- toUri:   URI of the destination node to start the computation at
-- threshold: minimum weight to take into account in computation
-- maxdepth: how many steps to walk from source/destination node; maximum distance considered between nodes is 2*maxdepth
-- ------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION shortest_path(fromUri TEXT, toUri TEXT, threshold DOUBLE PRECISION, maxdepth INT) RETURNS DOUBLE PRECISION AS $$
DECLARE
    fromId BIGINT;
    toId   BIGINT;
BEGIN
  SELECT id INTO fromId FROM nodes WHERE ntype = 'uri' AND svalue = fromUri;
  IF NOT FOUND THEN
    RETURN 0.0;
  END IF;

  SELECT id INTO toId FROM nodes WHERE ntype = 'uri' AND svalue = toUri;
  IF NOT FOUND THEN
    RETURN 0.0;
  END IF;


  RETURN shortest_path_ids(fromId, toId, threshold, maxdepth);
END
$$ LANGUAGE plpgsql;


-- compute shortest path between two nodes; computation is carried out by using a variation of a lowest common ancestor
-- algorithm where we start traversal at both the fromId and toId nodes and then walk along all edges until either maxdepth
-- is reached or no new nodes are found; then compute the sum of all possible paths in the (temporary) result set
CREATE OR REPLACE FUNCTION shortest_path_ids(fromId BIGINT, toId BIGINT, threshold DOUBLE PRECISION, maxdepth INT) RETURNS DOUBLE PRECISION AS $$
DECLARE
    weights RECORD;
    best    DOUBLE PRECISION := 0.0;

BEGIN

  -- special case
  IF fromId = toId THEN
    RETURN 1.0;
  END IF;

  -- cached result
  SELECT weight INTO best FROM shortest_path_cache WHERE (fromNode = fromId AND toNode = toId) OR (fromNode = toId AND toNode = fromId);
  IF FOUND THEN
    RETURN best;
  END IF;

  -- keep track of visited nodes; we store from which node we started which other node we reached with which distance
  CREATE TEMPORARY TABLE visited (
    fromNode BIGINT,
    toNode   BIGINT,
    weight   DOUBLE PRECISION,
    round    INT
  ) ON COMMIT DROP;
  CREATE INDEX idx_visited_from ON visited(fromNode);
  CREATE INDEX idx_visited_to   ON visited(toNode);


  -- loop 6 times, walking along the edges starting at the fromId and toId
  FOR i IN 0..maxdepth LOOP
    IF i = 0 THEN
      -- special case: distance to self is 1.0
      INSERT INTO visited VALUES (fromId,fromId,1.0,i), (toId,toId,1.0,i);

    ELSE
      INSERT INTO visited (
        SELECT DISTINCT V.fromNode, T.object, W.weight * V.weight * S.selectivity, i
        FROM triples T, weights W, visited V, selectivities S
        WHERE T.subject = V.toNode AND T.predicate = W.property AND T.deleted = false
          AND T.predicate = S.property AND T.object = S.id
          AND W.weight * V.weight * S.selectivity > threshold AND V.round = i-1
      );

      -- in case no more relations are added, we can terminate
      IF NOT FOUND THEN
        EXIT;
      END IF;
    END IF ;

  END LOOP;

-- check for existence of a match between fromId and toId
  SELECT sum(V1.weight * V2.weight) INTO best FROM visited V1, visited V2 WHERE V1.fromNode = fromId AND V1.toNode = V2.toNode AND V2.fromNode = toId;
  IF best > threshold THEN
    INSERT INTO shortest_path_cache VALUES (fromId, toId, best);
    DROP TABLE visited;

    RETURN best;
  ELSE
    INSERT INTO shortest_path_cache VALUES (fromId, toId, 0.0);
    DROP TABLE visited;

    RETURN 0.0;
  END IF;
END;
$$ LANGUAGE plpgsql;


