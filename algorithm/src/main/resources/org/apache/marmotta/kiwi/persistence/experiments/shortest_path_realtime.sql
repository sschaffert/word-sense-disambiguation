CREATE OR REPLACE FUNCTION shortest_path(fromUri TEXT, toUri TEXT, threshold DOUBLE PRECISION, maxdepth INT) RETURNS DOUBLE PRECISION AS $$
DECLARE
    fromId BIGINT;
    toId   BIGINT;
BEGIN
  SELECT id INTO fromId FROM nodes WHERE ntype = 'uri' AND svalue = fromUri;
  IF NOT FOUND THEN
    RETURN 0.0;
  END IF;

  SELECT id INTO toId FROM nodes WHERE ntype = 'uri' AND svalue = toUri;
  IF NOT FOUND THEN
    RETURN 0.0;
  END IF;

  RETURN shortest_path_ids(fromId, toId, threshold, maxdepth);
END
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION shortest_path_ids(fromId BIGINT, toId BIGINT, threshold DOUBLE PRECISION, maxdepth INT) RETURNS DOUBLE PRECISION AS $$
DECLARE
    weights RECORD;
    best    DOUBLE PRECISION := 0.0;
    child   DOUBLE PRECISION;

BEGIN
--  RAISE NOTICE 'calling shortest path computation between % and %, threshold %', fromId, toId, threshold;
  IF fromId = toId THEN
    RETURN 1.0;
  END IF;

  IF maxdepth = 0 THEN
    RETURN 0.0;
  END IF;

  FOR weights IN (SELECT T.object AS toNode, W.weight
                  FROM triples T, weights W
                  WHERE T.deleted = false AND T.predicate = W.property AND T.subject = fromId AND W.weight >= threshold)
                 UNION
                 (SELECT T.subject AS toNode, W.weight
                  FROM triples T, weights W
                  WHERE T.deleted = false AND T.predicate = W.property AND T.object = fromId AND W.weight >= threshold)
  LOOP
    child := shortest_path_ids(weights.toNode, toId, threshold / weights.weight, maxdepth - 1) * weights.weight;
    IF child > best THEN
      best := child;
    END IF;
  END LOOP;

--  RAISE NOTICE 'returning best result %', best;

  RETURN best;
END;
$$ LANGUAGE plpgsql;
